# COMS3200 - Assignment 3 Part C
# Author: Irsan Winarto
# Student I.D.: 44316983
import utilities
import http_webget as hw
import ftp_webget as fw


# this function checks command line arguments and return server and file URI's
args = utilities.get_arguments()
# get the protocol
protocol = args[0]
# use the right module depending on protocol
if protocol.lower() == 'http':
    hw.http_webget(args)
elif protocol.lower() == 'ftp':
    fw.ftp_webget(args)
