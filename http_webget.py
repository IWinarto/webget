# COMS3200 - Assignment 3 Part C
# Author: Irsan Winarto
# Student I.D.: 44316983
import sys
import socket
import utilities


# This function downloads the file given in args[2] from the host given in args[1]
def http_webget(args):
    # host name
    host = args[1]
    # file absolute path
    file_abs_path = args[2]

    # default to index.html if no file absolute path
    file_name = 'index.html'
    if file_abs_path is not '/':
        file_name = file_abs_path.rsplit('/', 1)[-1]

    # make GET message to request file from server host
    request = utilities.make_request(host, file_abs_path)

    # create socket
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        utilities.print_red('Failed to create socket')
        sys.exit()

    # Connect to remote server
    port = 80

    try:
        print('# Connecting to server ' + host)
        client_socket.connect((host, port))
    except socket.error:
        utilities.print_red('Cannot connect to server ' + host)
        sys.exit()

    # send HTTP request to server host
    try:
        print('# Sending request to server:')
        client_socket.sendall(request.encode('ascii'))
        # print to console
        header_fields = request.split('\r\n')
        for fields in header_fields:
            print('  ' + fields)
    except socket.error:
        utilities.print_red('Send failed')
        sys.exit()

    print('# Getting reply from server')
    buff_size = 4096
    reply = client_socket.recv(buff_size)

    print('# Extracting headers')
    headers, message = utilities.get_headers_and_body(reply)
    print('  ' + '=' * len(max(headers, key=len)))
    for field in headers:
        print('  ' + field)
    print('  ' + '=' * len(max(headers, key=len)))

    print('# Checking reply code')
    err_msg = utilities.check_reply(headers)
    if err_msg is not None:
        utilities.print_red('  ' + err_msg)
        sys.exit()

    # check if transfer-encoding == chunked (I don't have to demonstrate this)
    if utilities.check_is_chunked(headers):
        print('# Getting message chunks (Transfer-Encoding = chunked)')
        chunks = message
        message = b''
        # file size for printing purpose only
        file_size = 0

        while True:
            # extract actual data from chunks
            data, remaining_size, data_size, stop = utilities.get_combined_chunk_msg(chunks)

            # check if chunks is actually the terminating chunk
            if stop:
                break
            else:
                # concatenate data to the message to be written to file
                message += data
                file_size += data_size

            # the last chunk in chunks is not complete, get the rest of it
            while remaining_size > 0:
                # the remaining message in the last chunk in chunk
                chunk = client_socket.recv(remaining_size)
                # check if the new chunk is still lacking
                if len(chunk) <= remaining_size - len('\r\n'):
                    message += chunk
                    saved_len = len(chunk)
                # the missing message has been collected and it also contains \r
                elif len(chunk) == remaining_size - len('\r\n') + 1:
                    message += chunk[:-1]
                    saved_len = len(chunk[:-1])
                # the missing message has been collected and it also contains \r\n
                else:
                    message += chunk[:-2]
                    saved_len = len(chunk[:-2])

                # update file size and the remaining size of data missing
                file_size += saved_len
                remaining_size -= len(chunk)

            # previous chunk was not a terminating chunk; so get more chunk
            chunks = client_socket.recv(buff_size)

        # report file size
        print('  file size is ', end='')
        utilities.print_green('%d bytes.' % file_size)

    else:
        # transfer encoding is NOT chunked
        print('# Getting content length')
        content_length = utilities.get_content_length(headers)
        print('  file size is ', end='')
        utilities.print_green('%d bytes' % content_length)

        print('# Getting more message fragments if exist')
        while len(message) < content_length:
            fragment = client_socket.recv(content_length - len(message))
            message += fragment

    # close socket
    client_socket.close()

    print('# Saving http message body (in binary) to ', end='')
    utilities.print_green(file_name)
    save_file = open(file_name, 'bw')
    save_file.write(message)
    save_file.close()
