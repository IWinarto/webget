# COMS3200 - Assignment 2 Part B
# Author: Irsan Winarto
# Student I.D.: 44316983
import sys
import codecs


# This function checks the command line arguments and returns host and file URI's
def get_arguments():
    # check number of arguments
    if len(sys.argv) != 2:
        sys.exit("Usage: webget PROTOCOL://HOST/FILENAME")

    # get entire url
    url = sys.argv[1]

    # split uri into protocol and url
    tokens = str(url).split('://', 1)

    if len(tokens) < 2:
        sys.exit("Incomplete protocol and url combination")

    # split url into host and file path
    sub_tokens = tokens[1].split('/', 1)

    protocol = tokens[0]
    host = sub_tokens[0]
    # file abs path set to root by default
    file_abs_path = '/'
    if len(sub_tokens) > 1:
        file_abs_path += sub_tokens[1]

    if protocol.lower() != 'http' and protocol.lower() != 'ftp':
        sys.exit("Protocol must be http or ftp only.")

    return protocol, host, file_abs_path


# This function forms the http GET message
def make_request(host, file_abs_path):
    request_line = 'GET ' + file_abs_path + ' HTTP/1.1\r\n'
    request = request_line + 'Host: ' + host + '\r\n\r\n'

    return request


# This function returns the headers and message body from the http GET response
# Note: the given reply is the str()-translated reply
def get_headers_and_body(reply):
    # convert the byte http packet to string (note: \r\n gets converted to \\r\\n)
    # converting bytes to string also encloses the original string in b' '
    stream = codecs.decode(str(reply)[2:-1], 'unicode_escape')
    header_fields = []
    header_len_in_bytes = 0

    while True:
        # get one field at a time
        tokens = stream.split('\r\n', 1)
        # add header fields to list
        header_fields.append(tokens[0])
        # update header length in bytes (+2 characters because \r\n)
        header_len_in_bytes += len(tokens[0]) + 2
        # tokenise the rest of the headers
        stream = tokens[1]
        # check if the next two characters are \r\n, then header extraction is complete
        if tokens[1][:2] == '\r\n':
            break

    # final +2 because the end of header has another \r\n
    header_len_in_bytes += 2

    return header_fields, reply[header_len_in_bytes:]


# This function returns None if reply code in the headers is 200; else, it returns the reply code and reason
def check_reply(headers):
    # get the status line (i.e. first line of http reply)
    status_line = headers[0]
    # tokenise to get reply code and reason
    tokens = status_line.split(' ', 2)
    status_code = int(tokens[1])
    reason = tokens[2]

    # if the status is not OK (200), return the error message
    ret = None
    msg = str(status_code) + ' ' + reason
    if status_code != 200:
        ret = msg
    else:
        # report 200 OK
        print_green('  ' + msg)

    return ret


# This function returns the length in bytes of the message body
def get_content_length(headers):

    # length of message body and the encoding format
    length = 0

    for field in headers:
        # find content length in header
        if 'Content-Length' in field:
            tokens = field.split(' ')
            length = int(tokens[-1])

    return length


# This function returns true iff Transfer-Encoding = chunked
def check_is_chunked(headers):

    for field in headers:
        if 'Transfer-Encoding' in field:
            tokens = field.split(':', 1)
            if 'chunked' in tokens[1]:
                return True

    return False


def print_red(string, end='\n'):
    print('\033[31m' + string, end=end)
    # reset to original
    sys.stdout.write('\033[0m')


def print_green(string, end='\n'):
    print('\033[32m' + string, end=end)
    # reset to original
    sys.stdout.write('\033[0m')


# I don't have to demonstrate this
# This function extracts the chunked messages from the byte array chunk
# Require: the first CRLF-terminated substring is the hexadecimal size of the first message in chunk
def get_combined_chunk_msg(chunks):
    # convert byte array to string
    stream = codecs.decode(str(chunks)[2:-1], 'unicode_escape')
    # the message body in binary to be returned
    combined_body = b''
    # the size of missing chunk message to be returned
    remaining_size = 0
    # a flag indicating whether chunk is a terminating chunk
    stop = False

    while True:
        # split chunk string into chunk size and the rest
        temp = stream.split('\r\n', 1)

        # if stream still contains chunk size and data...
        if len(temp) == 2 and temp[1] != '':
            # chunk size
            chunk_size = int(temp[0], 16)
            # check if the given chunk is a terminating chunk
            if chunk_size == 0 and temp[1][:2] == '\r\n':
                stop = True
                break

            # the length of chunk size + '\r\n' to be skipped from chunk bytes
            skip_length = len(temp[0]) + len('\r\n')
            # save actual data for a maximum of chunk_size
            combined_body += chunks[skip_length: skip_length + chunk_size]
            # the length of the data read
            prev_len = len(chunks[skip_length: skip_length + chunk_size])
            # skip already processed data
            chunks = chunks[skip_length + chunk_size + 2:]
            stream = temp[1][chunk_size + 2:]

        # else stream does not contain chunk size and the last data has been read
        else:
            # if the length of the last data read is less than amount of data being sent...
            if prev_len < chunk_size:
                remaining_size += (chunk_size - prev_len) + len('\r\n')
            break

    data_size = len(combined_body)
    return combined_body, remaining_size, data_size, stop
