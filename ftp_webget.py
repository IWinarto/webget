# COMS3200 - Assignment 3 Part C
# Author: Irsan Winarto
# Student I.D.: 44316983
import utilities
import ftplib


# This function retrieves file from the ftp host and directory given in args
def ftp_webget(args):
    # ftp server hostname
    host = args[1]
    # get directory and file name
    file_info = args[2].rsplit('/', 1)
    directory = file_info[0]
    file_name = file_info[1]
    # username and password
    username = 'anonymous'
    password = 'coms3200@uq.edu.au'

    # print message to console
    print('# Retrieving ', end='')
    utilities.print_green(file_name, end='')
    print(' from ', end='')
    utilities.print_green(host + directory)

    exception_thrown = False
    try:
        # retrieve file from the ftp server
        ftp = ftplib.FTP(host)
        ftp.login(username, password)
        ftp.cwd(directory)
        ftp.retrbinary('RETR ' + file_name, open(file_name, 'wb').write)
        ftp.quit()

    except ftplib.all_errors as e:
        exception_thrown = True
        if 'Errno 11001' in str(e):
            # Unable to resolve host name
            utilities.print_red('Unable to connect to ' + host)
        else:
            print('# Received error response:')
            err_str = str(e)
            if file_name in err_str:
                # No such file name
                err_str = err_str.replace(' or directory', '')
            elif directory in err_str:
                # No such directory
                err_str = err_str.replace(' file or', '')
            # print error message in red
            utilities.print_red(err_str)

    # no exception; download was successful
    if exception_thrown is False:
        print('# ftp download was successful')
